"""
All elements used in the game inherit from this base element.
"""

class BaseElement:
    additional_variables = {}
    controls = []

    def __repr__(self):
        return f"{self.__class__.__name__} at {self.location()}"

    def location(self):
        return (self.y, self.x, self.height)

    def actually_move(self, *args):
        if isinstance(args[0], (tuple, list)):
            self.actually_move(*args[0])
        elif len(args) == 2:
            self.actually_move(tuple(args) + (self.height,))
        elif len(args) == 3:
            self.maze.layout[self.y][self.x].remove(self)
            self.y, self.x, self.height = args
            self.maze.layout[self.y][self.x].append(self)

    def fall_down(self, position):
        """
        Move self to the highest height at position where self may step on.
        """
        y, x = position[:2]

        elements_at_new_loc = self.maze.get_elements(y, x)

        # @INCOMPLETE: random max height
        for height in range(10, -1, -1):
            location = (y, x, height)
            callbacks = [(self.actually_move, location)]
            for e in elements_at_new_loc:
                if e == self:
                    continue
                result = e.may_step_on(self, location)
                if not result:
                    continue
                elif isinstance(result, list):
                    callbacks += result
                    execute_callbacks(callbacks)
                    return True
                else:
                    execute_callbacks(callbacks)
                    return True

    def try_move(self, new_location, return_callbacks_instead_of_moving=False):
        """
        Investigate what needs to happen for `self` to move to `new_location`. If
        `return_callbacks_instead_of_moving` is `False` make these things happen.
        Otherwise return the callbacks as a list of tuples.
        """

        if len(new_location) == 2:
            # If the specified location doesn't contain a height, we fill in self.height.
            new_location = (*new_location, self.height)
        y, x, height = new_location

        if not self.maze.in_maze(y, x):
            return False

        #print("trying to move to", new_location)
        # Self moving is the first movement that takes place,
        # so that callbacks like teleporting don't get overwritten.
        callbacks = [(self.actually_move, new_location)]
        elements_at_old_loc = self.maze.get_elements(self.y, self.x)
        elements_at_new_loc = self.maze.get_elements(y, x)

        #print("elements_at_old_loc: ", elements_at_old_loc)
        #print("elements_at_new_loc: ", elements_at_new_loc)

        # Check whether the element may leave the old location.
        for e in elements_at_old_loc:
            result = e.may_leave(self, new_location)
            if not result:
                return False
            elif isinstance(result, list):
                callbacks += result

        # Check whether the element has something to stand on at the new location.
        for e in elements_at_new_loc:
            result = e.may_step_on(self, new_location)
            if not result:
                continue
            elif isinstance(result, list):
                callbacks += result
                break
            else:
                break
        else:
            return False

        # Check whether the element isn't blocked on the new location.
        for e in elements_at_new_loc:
            result = e.may_enter(self, new_location)
            if not result:
                return False
            elif isinstance(result, list):
                callbacks += result

        #print(callbacks)

        if return_callbacks_instead_of_moving:
            return callbacks
        else:
            execute_callbacks(callbacks)
            return True

    def may_leave(self, element, location):
        return True

    def may_step_on(self, element, location):
        return False

    def may_enter(self, element, location):
        return True

    def won(self):
        # All elements need to return True for a game winning situation, so True is default
        return True

    @classmethod
    def init(cls, nr, file, maze):
        for i in range(nr):
            lst = file.readline().split(" ")
            element = cls()
            element.maze = maze
            element.y, element.x, element.height = map(int, lst[:3])

            for attr, kind in cls.additional_variables.items():
                if "location" == kind:
                    setattr(element, attr, tuple(map(int, lst[3].split(","))))

            maze.element_dict[cls].append(element)
            maze.layout[element.y][element.x].append(element)

    @classmethod
    def save(cls, file, elements):
        file.write(f"{cls.__name__} {len(elements)}\n")
        for element in elements:

            additional_info = ""
            for attr, kind in cls.additional_variables.items():
                if "location" == kind:
                    additional_info += " " + ",".join(map(str, getattr(element, attr)))

            file.write(f"{element.y} {element.x} {element.height}{additional_info}\n")

def execute_callbacks(callbacks):
    #print("executing callbacks...")
    for func, args in callbacks:
        func(args)
