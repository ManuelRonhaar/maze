"""
File for all elements that can be used in the game.
"""

from controls import GameControls
from base_element import *

class Void(BaseElement):
    """Impassable"""
    def may_enter(self, element, location):
        return False

class Floor(BaseElement):
    def may_step_on(self, element, location):
        return location[2] == self.height

    def may_enter(self, element, location):
        return location[2] >= self.height

    def move(self, location):
        # floors shouldn't ever get moved, so print warning
        assert False, "EHHHH, don't move the floor. wtf!"

    @classmethod
    def init(cls, nr, file, maze):
        for y in range(maze.height):
            lst = map(int, file.readline().split())
            for x, height in enumerate(lst):
                element = cls()
                element.maze = maze
                element.y, element.x, element.height = y, x, height
                maze.element_dict[cls].append(element)
                maze.layout[element.y][element.x].append(element)

    @classmethod
    def save(cls, file, elements):
        maze = elements[0].maze
        file.write(f"{cls.__name__} {len(elements)}\n")
        for y in range(maze.height):
            file.write(" ".join([str(e.height) for e in elements[maze.width*y:maze.width*(y+1)]])+"\n")

class Player(BaseElement):
    controls = [(50, "move")]

    def move(self, key):
        key_to_direction = {
            GameControls.UP:     (-1, 0),
            GameControls.DOWN:   (1, 0),
            GameControls.RIGHT:  (0, 1),
            GameControls.LEFT:   (0, -1)
        }

        if key in key_to_direction:
            dy, dx = key_to_direction[key]
        else:
            return False

        new_pos = (self.y + dy, self.x + dx)
        return self.try_move(new_pos)

    def may_enter(self, element, location):
        return False

class Block(BaseElement):

    def may_step_on(self, element, location):
        # If the element is just above the block, it can step onto it.
        return location[2]  == self.height+1

    def may_enter(self, element, location):
        # If another block is trying to pass, it would be too heavy to push, so don't budge.
        if location[2] == self.height and isinstance(element, Block):
            return False

        # If the element is at the same height, the block would need to be pushed away
        # in the same direction as the pushing element is trying to move.
        elif location[2] == self.height and not isinstance(element, Block):
            new_pos = (2*self.y-element.y, 2*self.x-element.x)
            return self.try_move(new_pos, return_callbacks_instead_of_moving=True)

        # If the element is at a different height, the block doesn't have to move and the
        # element can pass.
        else:
            return True

class Finish(BaseElement):
    def won(self):
        elements = self.maze.get_elements(self.y, self.x)
        for e in elements:
            if isinstance(e, Player):
                return True
        return False

class EmptyFinish(Finish):
    def won(self):
        elements = self.maze.get_elements(self.y, self.x)
        for e in elements:
            if isinstance(e, Player) or isinstance(e, Block):
                return False
        return True

class Teleporter(BaseElement):
    additional_variables = {"destination": "location"}

    # REFACTOR POSSIBILITY: check at the end of every turn whether players have moved onto the
    # teleporter. Might be less vague, as then teleportation only happens after all movement.
    def may_enter(self, element, location):
        if isinstance(element, Player):
            return [(element.fall_down, self.destination)]
        else:
            return True

class CrazyTeleporter(BaseElement):
    additional_variables = {"destination": "location"}
    controls = [(0, "reset"), (100, "teleport")]

    def reset(self, key):
        self.teleported = False
        self.previous_players = self.maze.get_cls_elements_at(self.y, self.x, Player)

    def teleport(self, key):
        current_players = self.maze.get_cls_elements_at(self.y, self.x, Player)

        if current_players and not current_players.issubset(self.previous_players) and not self.teleported:
            y, x = self.destination[0] - self.y, self.destination[1] - self.x
            cb = [(p.fall_down, (p.y + y, p.x + x)) for p in self.maze.element_dict[Player]]
            execute_callbacks(cb)

            self.teleported = True
            dest = self.maze.get_cls_element_at(*self.destination, CrazyTeleporter)
            if dest:
                dest.teleported = True
