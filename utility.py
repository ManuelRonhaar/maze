"""used in other files for interacting with files and user"""

import os
import sys

def print_files(directory="layouts"):
    """print all available mazes"""
    print("Possible mazes are: " + ", ".join(get_files()))

def get_files(directory="layouts"):
    """generate all maze names"""
    for f in os.listdir(directory):
        if f[-4:] == ".txt":
            yield f[0:-4]

def clear_directory(directory):
    """remove all files from a directory"""
    for f in os.listdir(directory):
        os.remove(directory + "/" + f)

def exists(mazename, directory="layouts"):
    """check whether a maze exists"""
    return os.path.exists(f"{directory}/{mazename}.txt")

def remove(mazename, directory="layouts"):
    """remove maze with specified name"""
    os.remove(f"{directory}/{mazename}.txt")

def ask_maze():
    """ask the user for the maze they want to play
       until they give an existing maze"""
    while True:
        mazename = input("Which maze do you want to play? ")
        if exists(mazename):
            return mazename
        else:
            print("No such maze exists")
            print_files()

def ask_any_maze():
    """ask the user for a maze name (doesn't have to exist)"""
    return input("Please input a name ")

def ask_size():
    while True:
        size = input("Which height and width should the level be? (e.g. '5 5') ")
        try:
            t = tuple(map(int, size.split()))
            assert len(t) == 2
            return t
        except Exception:
            print("Invalid input. Should be 2 ints seperated by a space.")
            continue

def get_cmd_argument():
    if len(sys.argv) == 2:
        return sys.argv[1]
    return None
