from copy import copy, deepcopy

from controls import GameControls
from utility import *
from maze import *

DEFAULT_FASTEST_WIN = 1000000

class Game:
    def __init__(self, maze):
        self.maze = maze

        self.won = False
        self.fastest_win = DEFAULT_FASTEST_WIN
        self.turn = 0
        self.input_sequence = []
        self.base_maze = deepcopy(maze)

    def handle_game_input(self, key):
        """Handle all element inputs (everything except control flow)."""
        # Every turn is represented by multiple subturns. In each subturn
        # elements can execute an action. e.g.: players move in subturn 50 and
        # teleporters move in subturn 100. Only once all players have moved
        # do the teleporters trigger. An element can have actions in multiple
        # subturns. For every subturn a set is made containing the (element, method)
        # pairs that should be called that subturn.
        #
        # Obviously all these methods should be called. But in what way?
        # At first they were all called once in some order, which caused
        # players to block eachother inconsistantly. Consider the scenario where
        # 2 players stand next to eachother and GameControls.RIGHT is issued.
        #       original: PLAYER PLAYER EMPTY
        #       if right player gets called first: EMPTY PLAYER PLAYER
        #       if left player gets called first:  PLAYER EMPTY PLAYER
        # This happens as the left player is blocked by the right player
        # depending on the calling order. To mitigate this issue the player move
        # function was altered to "push" other players. This worked, but was
        # ugly and not scalable, so an alternate solution was sougth.
        # Two potential solutions were considered:
        #       1. Call the methods in a specific order. e.g: if
        #       GameControls.RIGHT gets issued, call move from right to left
        #       and for GameControls.LEFT the other way around.
        #       2. Call not yet succeeded methods untill all have succeeded or
        #       untill all have been called without any changes to the state.
        # Both seem nicer than the original. Option 2 was chosen as this
        # requires less strange hard coding. Still not everything is perfect;
        # imagine IPlayer, which behaves like Player except that it moves in
        # the opposite direction. Now consider a situation where a player and
        # iplayer both want to move to the same tile.
        #       PLAYER EMPTY IPLAYER and GameControls.RIGHT gets issued
        # What would happen depends on the calling order and is not well
        # defined. To prevent such undifined behaviour, Elements that are called
        # in the same subturn should not move to different sides.

        # Keep track of whether something changed as a result of the pressed key.
        changed = False

        for subturn, actions in self.maze.turn_functions.items():

            # Which (element, method) pairs should be called this subturn.
            to_do = set()
            for (cls, method) in actions:
                for element in self.maze.element_dict[cls]:
                    to_do.add((element, method))

            while to_do:

                not_done = set()
                for element, method in to_do:
                    if not method(element, key):
                        # The action could not be executed.
                        not_done.add((element, method))

                if to_do == not_done:
                    # No actions could be executed this subturn.
                    break
                else:
                    to_do = not_done
                    changed = True

        return changed

    def handle_control_input(self, key, changed):
        """Save and load according to pressed key."""
        if changed:
            self.turn += 1
            self.save_game(key)
        if key == GameControls.UNDO:
            self.load_game(self.turn - 1)
            return True
        if key == GameControls.REDO:
            self.load_game(self.turn + 1)
            return True
        if key == GameControls.RESTART:
            self.won = False
            self.load_game(0)
            return True

    def save_game(self, key):
        if len(self.input_sequence) >= self.turn:
            self.input_sequence = self.input_sequence[:self.turn-1]
        self.input_sequence.append(key)

    def load_game(self, turn):
        if 0 <= turn <= len(self.input_sequence):
            self.maze = deepcopy(self.base_maze)
            for t in range(turn):
                self.handle_game_input(self.input_sequence[t])
            self.turn = turn

    def update_won(self):
        """Check whether all win conditions are met, and update corresponding variables."""
        for element in self.maze.element_iter():
            if not element.won():
                return
        self.won = True
        self.fastest_win = min(self.fastest_win, self.turn)

    def step(self, key):
        """Update the maze and visualize it."""
        changed = self.handle_game_input(key)
        changed = self.handle_control_input(key, changed) or changed
        self.update_won()
        return changed

if __name__ == "__main__":
    from pgame.controller import Controller
    Controller().main()
