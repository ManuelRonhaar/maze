requirements:
    - python 3.6+ (built using 3.8) (https://www.python.org/downloads/release/python-386/ (remember to add to PATH))
    - pygame 2 (built using 2.0) (https://www.pygame.org/wiki/GettingStarted)

play_sequence.py provides 12 levels to play in quickly increasing difficulty.
controller.py gives the possibility to play all levels and to make levels (press l to go to the editor)

controls:
    Game:
        - arrow keys to move the players (red dots) around
        - 0 to reset
        - [, ] to undo and redo
    LayoutMaker:
        - s for save
        - mouse determines position of changes
        - mouse left click makes a tile black
        - mouse right click makes a tile white
        - p for players
        - f for finish
        - b for block
        - t for teleporter
        - e for empty finish (blue tile)
        - o for opponent
    PlaySequence:
        - PAGEUP for next level
        - PAGEDOWN for previous level
        - HOME for level 0
    Controller:
        - g to switch to game
        - l to switch to layoutmaker
