from enum import Enum, auto

class GameControls(Enum):
    UP                      = 0
    DOWN                    = 1
    LEFT                    = 2
    RIGHT                   = 3

    ALTERNATIVE_UP          = 4
    ALTERNATIVE_DOWN        = 5
    ALTERNATIVE_LEFT        = 6
    ALTERNATIVE_RIGHT       = 7

    SPECIAL                 = 8

    NONE                    = auto()
    RESTART                 = auto()
    UNDO                    = auto()
    REDO                    = auto()

class LayoutMakerControls(Enum):
    """
    Might not need this if layoutmaker is dependent on platform either way.
    """
    pass

class SequenceControls(Enum):
    NEXT_LEVEL              = auto()
    PREVIOUS_LEVEL          = auto()
    FIRST_LEVEL             = auto()

class ControllerControls(Enum):
    LAYOUT_MAKER            = auto()
    CURRENT_LAYOUT_MAKER    = auto()
    GAME                    = auto()
