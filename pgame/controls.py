"""
Maps pygame controls to game controls.
This is added so that element can use controls like UP without worrying about
the corresponding pygame key.
"""

import pygame
from elements import *
from controls import *

won = pygame.USEREVENT+1

game_controls = {
    pygame.K_r:                     GameControls.RESTART,
    pygame.K_z:                     GameControls.UNDO,
    pygame.K_LEFTBRACKET:           GameControls.UNDO,
    pygame.K_RIGHTBRACKET:          GameControls.REDO,

    pygame.K_UP:                    GameControls.UP,
    pygame.K_DOWN:                  GameControls.DOWN,
    pygame.K_LEFT:                  GameControls.LEFT,
    pygame.K_RIGHT:                 GameControls.RIGHT,

    pygame.K_w:                     GameControls.ALTERNATIVE_UP,
    pygame.K_s:                     GameControls.ALTERNATIVE_DOWN,
    pygame.K_a:                     GameControls.ALTERNATIVE_LEFT,
    pygame.K_d:                     GameControls.ALTERNATIVE_RIGHT,

    pygame.K_SPACE:                 GameControls.SPECIAL
}

layout_maker_controls = {
    pygame.K_v:                     Void,
    pygame.K_p:                     Player,
    pygame.K_f:                     Finish,
    pygame.K_b:                     Block,
    pygame.K_t:                     Teleporter,
    pygame.K_e:                     EmptyFinish,
    pygame.K_c:                     CrazyTeleporter,
}

sequence_controls = {
    pygame.K_PERIOD:                SequenceControls.NEXT_LEVEL,
    pygame.K_COMMA:                 SequenceControls.PREVIOUS_LEVEL,
    pygame.K_0:                     SequenceControls.FIRST_LEVEL
}

controller_controls = {
    pygame.K_l:                     ControllerControls.LAYOUT_MAKER,
    pygame.K_c:                     ControllerControls.CURRENT_LAYOUT_MAKER,
    pygame.K_g:                     ControllerControls.GAME
}
