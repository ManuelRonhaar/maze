import pygame

from pgame.visualizer import *
from pgame.controls import game_controls, won
from controls import GameControls
from game import *

class PGame(Game):
    def __init__(self, maze, visualizer, listener=None):
        super().__init__(maze)
        self.visualizer = visualizer
        self.listener = listener
        self.keys_queue = []
        self.won_before = False

    def determine_key(self, pressed_is_good_enough=False):
        """determine the one key that will be used for updating the game"""
        if self.keys_queue:
            return game_controls[self.keys_queue.pop(0)]

        if pressed_is_good_enough:
            # maybe prioritize later pressed keys
            keys = pygame.key.get_pressed()
            for key in game_controls:
                if keys[key]:
                    return game_controls[key]

        return GameControls.NONE

    def add_keys_to_queue(self, event):
        """save keys that are pressed for later checking"""
        if event.type == pygame.KEYDOWN and event.key in game_controls:
            self.keys_queue.append(event.key)

    def main(self):
        """keep the game running at 60 fps"""
        frames_till_pressed_works = -1

        self.visualizer.visualize(self.maze, extra=[self.visualizer.vis_won] * self.won)
        clock = pygame.time.Clock()

        while True:
            changed = False
            clock.tick(60)
            if frames_till_pressed_works >= 0: frames_till_pressed_works -= 1
            handler_result = self.visualizer.handle_events([self.add_keys_to_queue, self.listener])
            if not handler_result:
                return

            if frames_till_pressed_works < 10:
                key = self.determine_key(frames_till_pressed_works<=0)
                if key != GameControls.NONE:
                    frames_till_pressed_works = 15
                    changed = self.step(key)
                    if self.won and not self.won_before:
                        self.won_before = True
                        pygame.time.set_timer(pygame.event.Event(won), 600, 1)

            # Redraw if state changed or window format changed.
            if changed or handler_result == "redraw":
                self.visualizer.visualize(self.maze, extra=[self.visualizer.vis_won] * self.won)
                self.visualizer.append_caption(f", turn {self.turn}" +
                                f" and best solve in turn {self.fastest_win}"*(self.fastest_win!=DEFAULT_FASTEST_WIN))

def main(filename=None):
    """initialize game and keep it running at 60 fps"""
    # shouldn't get used as everything goes via Controller or PlaySequence
    maze = Maze(ask_maze())
    visualizer = Visualizer(maze)
    game = Game(maze, visualizer)
    game.main()
    pygame.quit()

if __name__ == "__main__":
    print("Arrows move the red dots. Get them on the green squares to win. "
            "[, ] are undo and redo, 0 will reset.")
    from pgame.controller import Controller
    controller = Controller()
    controller.main()
