import pygame
from copy import deepcopy

from pgame.game import *
from pgame.layout_maker import *
from pgame.controls import controller_controls
from controls import ControllerControls
from maze import *
from utility import *

class Controller:
    """switch between different main classes of maze (game/layoutmaker)"""

    def __init__(self, name=None, maze=None, visualizer=None, start_game=True):
        # Either name should be a valid maze name or maze should be a Maze.
        if not maze and not exists(name):
            print_files()
            name = ask_any_maze()
            if not exists(name):
                size = ask_size()
                maze = Maze(name, size=size)
        self.maze = maze if maze else Maze(name)
        self.visualizer = visualizer if visualizer else Visualizer(self.maze)
        self.layoutmaker = LayoutMaker(self.maze, self.visualizer, listener=self.listen_for_switch)
        self.active = self.layoutmaker if not start_game or self.maze.new else \
                PGame(deepcopy(self.maze), self.visualizer, listener=self.listen_for_switch)
        self.queued = None
        self.key_to_cls = { ControllerControls.LAYOUT_MAKER: LayoutMaker,
                            ControllerControls.CURRENT_LAYOUT_MAKER: LayoutMaker,
                            ControllerControls.GAME: Game }

    def listen_for_switch(self, event):
        """ if the event is a switch to another main class:
                - queue the new class
                - post a pygame.quit event
            this way the main loop will run the new class
        """
        if event.type == pygame.KEYDOWN:
            if event.key in controller_controls:
                to_cls = self.key_to_cls[controller_controls[event.key]]
                if isinstance(self.active, to_cls):
                    return

                elif to_cls is Game:
                    self.queued = PGame(deepcopy(self.active.maze), self.visualizer, \
                                                listener=self.listen_for_switch)
                elif controller_controls[event.key] == ControllerControls.LAYOUT_MAKER:
                    self.queued = self.layoutmaker
                elif controller_controls[event.key] == ControllerControls.CURRENT_LAYOUT_MAKER:
                    # LayoutMaker for the current situation of game.maze
                    self.queued = LayoutMaker(self.active.maze, self.visualizer, listener=self.listen_for_switch)

                pygame.event.post(pygame.event.Event(pygame.QUIT))

    def main(self):
        """ Keep running self.active
            If it stops (due to pygame.quit) run self.queued or quit
        """
        while True:
            self.visualizer.set_caption("Manuzzle " + self.maze.name +
                                        " " + type(self.active).__name__)
            self.active.main()
            if self.queued:
                self.active = self.queued
                self.queued = None
            else:
                pygame.quit()
                break

if __name__ == "__main__":
    controller = Controller(get_cmd_argument())
    controller.main()
