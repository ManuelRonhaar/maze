"""This file will run a sequence of levels"""

import pygame

from pgame.game import *
from pgame.controls import sequence_controls, won
from controls import SequenceControls

# Maybe go to next lvl if won?

class PlaySequence:
    """play a sequence of lvls"""
    sequences = {
        "standard": ["m", "makkelijk", "een_rand", "w", "wowi", "hallo", "hahi", "hoho", "wow", "you_won"],
        "teleporter": ["het_werkt", "een", "np", "p", "namen"],
        "emptyFinish": ["empty_tutorial", "emptyRow", "blijkbaarMogelijk"],
        "switch": ["switchIntro", "e", "c", "echtMoeilijk"],
        "all": list(get_files())
    }

    def __init__(self, sequence=None):
        if not sequence or sequence not in self.sequences: sequence = "standard"
        self.visualizer = Visualizer(Maze("w"))
        self.games = [PGame(Maze(mazename), self.visualizer, self.listen_for_switch) \
                                            for mazename in self.sequences[sequence]]
        self.counter = 0  # keep track of which lvl is being played
        self.fastest_wins = [DEFAULT_FASTEST_WIN] * len(self.games)

    def listen_for_switch(self, event):
        """ if the event is a switch to another maze:
                - change the counter
                - post a pygame.quit event
            this way the main loop will play the new maze
        """
        counter = self.counter

        if event.type == pygame.KEYDOWN:
            if event.key in sequence_controls:
                # update the counter
                action = sequence_controls[event.key]
                if action == SequenceControls.PREVIOUS_LEVEL:
                    counter = max(0, self.counter-1)
                elif action == SequenceControls.NEXT_LEVEL:
                    counter = min(len(self.games)-1, self.counter+1)
                elif action == SequenceControls.FIRST_LEVEL:
                    counter = 0
        elif event.type == won:
            counter = min(len(self.games)-1, self.counter+1)

        if counter != self.counter:
            # Counter was changed by the event. Throw a quit event, so
            # the new game can be started up.
            self.counter = counter
            pygame.event.post(pygame.event.Event(pygame.QUIT, {}))

    def main(self):
        """run the game of maze at self.counter"""
        while True:
            # counter will get updated in a callback, so we need to save the value
            counter = self.counter
            game = self.games[counter]
            self.visualizer.set_caption(f"Manuzzle {game.maze.name}"
                                f" {counter + 1}/{len(self.games)}")
            self.visualizer.change_maze(game.maze)
            game.main()
            self.fastest_wins[counter] = game.fastest_win

            # game.main returning could mean that a different maze should be played
            # (pygame.QUIT was posted by listen_for_switch)
            if self.counter != counter:
                continue
            # or it could mean that the user actually wants to quit
            else:
                self.print_score()
                pygame.quit()
                break

    def print_score(self):
        print("\nlevel    amount of turns")

        total_turns = 0
        total_won = 0
        for i, fastest_win in enumerate(self.fastest_wins):
            if fastest_win != DEFAULT_FASTEST_WIN:
                print(f"{str(i+1).rjust(3)}. {str(fastest_win).rjust(13)}")
                total_turns += fastest_win
                total_won += 1
            else:
                print(f"{str(i+1).rjust(3)}. not completed")
        print(f"\nyou completed {total_won}/{len(self.fastest_wins)} of the puzzles in {total_turns} turns")

if __name__ == "__main__":
    print("Arrow keys move the red dots. Get them all to green squares to win")
    print("[, ] is undo and redo. 0 will reset the maze to the start")
    print("PAGEUP is next, PAGEDOWN is previous, HOME resets to lvl 1")
    p = PlaySequence(get_cmd_argument())
    p.main()
