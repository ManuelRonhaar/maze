import pygame

from pgame.visualizer import Visualizer
from pgame.controls import layout_maker_controls
from utility import *
from maze import *

# add possibility to work with save_attributes
# maybe GUI?

class LayoutMaker:
    def __init__(self, maze=None, visualizer=None, name=None, listener=None):
        """ if a name is given, this is used to generate the mazes
            else the given maze and visualizer are get_mouse_coordinates"""
        if name:
            self.maze = Maze(name)
            self.visualizer = visualizer if visualizer else \
                    Visualizer(self.maze, caption=name+" maker")
        else:
            self.maze = maze
            self.visualizer = visualizer
        self.listener = listener
        self.energy_saver = False
        self.to_extend_elements = {cls: None for cls in self.maze.element_classes if "location" in cls.additional_variables.values()}

        #self.keydown_elements = self.maze.map_keydown_to_class()
        #self.pressed_elements = self.maze.map_pressed_to_class()
        #self.add_elements = dict.fromkeys(self.pressed_elements.values(), True)

    def handle_mouse_and_kb(self):
        """handles floor creation"""
        # change the floor
        press = pygame.mouse.get_pressed()
        if press[0] or press[2]:
            for i, e in enumerate(self.maze.layout[self.y][self.x]):
                if isinstance(e, Floor):
                    break
            del self.maze.layout[self.y][self.x][i]
            del self.maze.element_dict[Floor][self.maze.element_dict[Floor].index(e)]

            element = Floor()
            element.maze = self.maze
            element.y = self.y
            element.x = self.x
            element.height = int(press[0])

            self.maze.element_dict[Floor].append(element)
            self.maze.layout[self.y][self.x].append(element)

        # # placing elements by PRESSED
        # keys = pygame.key.get_pressed()
        # for key in self.pressed_elements:
        #     if keys[key]:
        #         self.update(self.pressed_elements[key], keydown=False)

    def update(self, cls):
        """update the existence of element"""

        # test whether the element is already on mouse position
        for i, element in enumerate(self.maze.element_dict[cls]):
            if element.y == self.y and element.x == self.x:
                break
        else: element = None

        # place element
        if element is None:
            element = cls()
            element.maze = self.maze
            element.y = self.y
            element.x = self.x
            element.height = 0

            # @INCOMPLETE: no good way of adding additional attributes
            if cls in self.to_extend_elements:
                other = self.to_extend_elements[cls]
                if other:
                    self.to_extend_elements[cls] = None
                    other.destination = (element.y, element.x)
                    element.destination = (other.y, other.x)
                else:
                    self.to_extend_elements[cls] = element
                    element.destination = (element.y, element.x)

            self.maze.element_dict[cls].append(element)
            self.maze.layout[self.y][self.x].append(element)
            element.fall_down((self.y, self.x))

        # delete element
        else:
            del self.maze.element_dict[cls][i]
            del self.maze.layout[self.y][self.x][self.maze.layout[self.y][self.x].index(element)]

    def handle_event(self, event):
        """handle the pygame events: mainly pressing keys"""
        if event.type == pygame.KEYDOWN:

            # saving
            if event.key == pygame.K_s:
                self.maze.save(self.maze.name)

            if event.key == pygame.K_0:
                self.maze.generate_empty_maze((self.maze.width, self.maze.height))

            # placing elements by KEYDOWN
            if event.key in layout_maker_controls:
                self.update(layout_maker_controls[event.key])

            # # toggling add/remove for elements placed with PRESSED
            # for key in self.pressed_elements:
            #     if event.key == key:
            #         cls = self.pressed_elements[key]
            #         self.add_elements[cls] = True
            #         for element in self.maze.elements_dict[cls]:
            #             if element.y == self.y and element.x == self.x:
            #                 self.add_elements[cls] = False
            #                 break

        # elif event.type == pygame.WINDOWEVENT:
        #     if event.event == pygame.WINDOWEVENT_LEAVE:
        #         self.energy_saver = True
        #     if event.event == pygame.WINDOWEVENT_ENTER:
        #         self.energy_saver = False

    def step(self):
        """run one frame of the layoutmaker; also includes visualizing"""
        self.y, self.x = self.visualizer.get_mouse_coordinates()
        if not self.visualizer.handle_events([self.handle_event, self.listener]): return "stop"
        self.handle_mouse_and_kb()
        self.visualizer.visualize(self.maze)

    def main(self):
        """keep running self"""
        while True:
            if self.step() == "stop":
                break
            if self.energy_saver:
                pygame.time.wait(1000)

def main(layout_maker=None):
    """create layout maker and keep running it"""
    from controller import Controller
    controller = Controller(start_game=False)
    controller.main()

if __name__ == "__main__":
    main()
