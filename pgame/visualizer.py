import pygame
from math import ceil, floor

from utility import get_files
from maze import *

# @TODO: make every tile square no matter the dimension of the window

class Visualizer:
    maze_colors = [(255, 255, 255), (0, 0, 0)]
    # [empty, wall]

    def __init__(self, maze, width=500, caption="Maze"):
        self.maze = maze
        self.tiles_y, self.tiles_x = maze.height, maze.width
        self.width = width
        self.height = int(self.width * self.tiles_y / self.tiles_x)
        self.tile_width = self.width / self.tiles_x
        self.tile_height = self.height / self.tiles_y
        self.draw_list = [
            (Floor, self.draw_floor, {"color": (0, 0, 0), "size": 1}),
            (Void, self.draw_square, {"color": (100, 10, 100), "size": 1}),
            (Block, self.draw_square, {"color": (100, 100, 100), "size": 1}),
            # (Switch, self.draw_square, {"color": (255, 0, 255), "size": 0.9}),
            # (PressurePlate, self.draw_square, {"color": (255, 100, 255), "size": 0.9}),
            # (MazeSwitch, self.draw_square, {"color": (255, 50, 255), "size": 0.9}),
            (Finish, self.draw_finish, {"color": (0, 255, 0), "size": 1}),
            (EmptyFinish, self.draw_finish, {"color": (0, 255, 255), "size": 1}),
            (Teleporter, self.draw_circle, {"color": (0, 0, 255), "size": 0.7}),
            (CrazyTeleporter, self.draw_circle, {"color": (30, 0, 255), "size": 0.7}),
            (Teleporter, self.draw_connection, {"color": (0, 0, 255), "thickness": 2}),
            (CrazyTeleporter, self.draw_connection, {"color": (30, 0, 255), "thickness": 2}),
            # (AlternativePlayer, self.draw_circle, {"color": (255, 0, 255), "size": 0.9}),
            (Player, self.draw_circle, {"color": (255, 0, 0), "size": 0.9}),
            # (Enemy, self.draw_enemy, {"color": (255, 170, 0), "size": 0.8}),
            # (Enemy, self.draw_connection, {"color": (255, 170, 0), "thickness": 2}),
        ]

        pygame.init()
        self.window = pygame.display.set_mode((self.width, self.height),
                    flags=pygame.RESIZABLE | pygame.SHOWN)
        self.base_caption = caption
        pygame.display.set_caption(caption)

    def set_caption(self, caption):
        self.base_caption = caption
        pygame.display.set_caption(caption)

    def append_caption(self, appendation):
        pygame.display.set_caption(self.base_caption + appendation)

    def change_maze(self, maze):
        """call this function if the visualizer should visualize a different maze"""
        self.maze = maze
        self.tiles_y, self.tiles_x = maze.height, maze.width
        new_height = int(self.width * self.tiles_y / self.tiles_x)
        if new_height != self.height:
            self.height = new_height
            self.window = pygame.display.set_mode((self.width, self.height),
                        flags=pygame.RESIZABLE | pygame.SHOWN)
        self.tile_width = self.width / self.tiles_x
        self.tile_height = self.height / self.tiles_y

    def visualize(self, maze, extra=[]):
        #self.draw_layout(maze.layout)
        if maze != self.maze:
            self.change_maze(maze)
        for cls, func, kwargs in self.draw_list:
            for element in maze.element_dict[cls]:
                func(element, **kwargs)
        for func in extra:
            func(self.window, self.width, self.height)
        pygame.display.update()

    def get_area(self, element, size):
        asize = 1 - size
        x_start = self.tile_width * element.x
        x_end = self.tile_width * (element.x + 1)
        x_actual_start = ceil(x_start*size + x_end*asize)
        x_actual_end = ceil(x_start*asize + x_end*size - x_actual_start)
        y_start = self.tile_height * element.y
        y_end = self.tile_height * (element.y + 1)
        y_actual_start = ceil(y_start*size + y_end*asize)
        y_actual_end = ceil(y_start*asize + y_end*size - y_actual_start)
        return (x_actual_start, y_actual_start, x_actual_end, y_actual_end)

    def get_center(self, element=None, coordinates=(0, 0)):
        if element:
            y, x = element.y, element.x
        else:
            y, x = coordinates
        y_center = (self.tile_width*y + self.tile_width*(y+1))*0.5
        x_center = (self.tile_width*x + self.tile_width*(x+1))*0.5
        return (ceil(x_center), ceil(y_center))

    def draw_circle(self, element, color=(0, 0, 0), size=1):
        pygame.draw.ellipse(self.window, color, self.get_area(element, size))

    def draw_floor(self, element, color=(0, 0, 0), size=1):
        if element.height == 0: color = (255, 255, 255)
        pygame.draw.rect(self.window, color, self.get_area(element, size))

    def draw_square(self, element, color=(0, 0, 0), size=1):
        pygame.draw.rect(self.window, color, self.get_area(element, size))

    def draw_finish(self, element, color=(0, 0, 0), size=1):
        x, y, width, height = self.get_area(element, size)
        pygame.draw.polygon(self.window, color, (
            (x + width / 8, y),
            (x + width * 3 / 8, y),
            (x, y + height * 3 / 8),
            (x, y + height / 8)
        ))
        pygame.draw.polygon(self.window, color, (
            (x + width * 5 / 8, y),
            (x + width * 7 / 8, y),
            (x, y + height * 7 / 8),
            (x, y + height * 5 / 8)
        ))
        # idk why the -1 is necessary, but otherwise it crosses the border
        pygame.draw.polygon(self.window, color, (
            (x + width / 8, y + height - 1),
            (x + width * 3 / 8, y + height - 1),
            (x + width - 1, y + height * 3 / 8),
            (x + width - 1, y + height / 8)
        ))
        pygame.draw.polygon(self.window, color, (
            (x + width * 5 / 8, y + height - 1),
            (x + width * 7 / 8, y + height - 1),
            (x + width - 1, y + height * 7 / 8),
            (x + width - 1, y + height * 5 / 8)
        ))

    def draw_enemy(self, element, color=(0, 0, 0), size=1):
        x, y, width, height = self.get_area(element, size)
        pygame.draw.polygon(self.window, color,
                            ((x + width // 2, y),
                             (x, y + height // 2),
                             (x + width // 2, y + height),
                             (x + width, y + height // 2)))

    def draw_connection(self, element, color=(0, 0, 0), thickness=1):
        pygame.draw.line(self.window, color, self.get_center(element),
                         self.get_center(coordinates=element.destination), thickness)

    def vis_won(self, window, width, height):
        """visualize winning the game"""
        font = pygame.font.SysFont("Arial", 60)
        font_height = font.get_height()
        pygame.draw.rect(window, (0, 0, 0), (0, int((height - font_height) / 2),
                                             width, font_height + 1))
        surface = font.render("YOU WON", True, (255, 0, 0), (0, 0, 0))
        rectangle = surface.get_rect()
        rectangle.center = (int(width / 2), int(height / 2))
        window.blit(surface, rectangle)

    def get_mouse_coordinates(self) -> tuple:
        x, y = pygame.mouse.get_pos()
        return (floor(y / self.tile_height)), (floor(x / self.tile_width))

    def handle_events(self, extra_handlers=[]):
        """handle all events, return False if there is a need to stop"""
        result = True
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return False
            elif event.type == pygame.VIDEORESIZE:
                self.width = event.w
                self.tile_width = event.w / self.tiles_x
                self.tile_height = self.tile_width
                self.height = int(self.width * self.tiles_y / self.tiles_x)
                self.window = pygame.display.set_mode((self.width, self.height),
                                flags=pygame.RESIZABLE | pygame.SHOWN)
                result = "redraw"
            elif event.type == pygame.VIDEOEXPOSE:
                result = "redraw"
            elif extra_handlers:
                for handler in extra_handlers:
                    if handler is not None:
                        handler(event)
        else:
            return result
