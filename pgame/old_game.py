import pygame

from pgame.visualizer import *
from pgame.controls import game_controls
from controls import GameControls
from game import *

class PGame(Game):
    def __init__(self, maze, visualizer, listener=None):
        super().__init__(maze)
        self.visualizer = visualizer
        self.listener = listener
        self.saved_keys = []

    def determine_key(self):
        """determine the one key that will be used for updating the game"""
        for key in self.saved_keys:
            if key in game_controls:
                self.saved_keys.clear()
                return game_controls[key]
        self.saved_keys.clear()

        keys = pygame.key.get_pressed()
        for key in game_controls:
            if keys[key]:
                return game_controls[key]

        return GameControls.NONE

    def save_keys(self, event):
        """save keys that are pressed for later checking"""
        # TODO: change to instant change on event
        if event.type == pygame.KEYDOWN:
            self.saved_keys.append(event.key)

    def step(self):
        """update the maze and visualize it"""
        stop = not self.visualizer.handle_events([self.save_keys, self.listener])

        key = self.determine_key()
        self.handle_input(key)
        self.check_won()
        self.visualizer.visualize(self.maze, extra=[self.visualizer.vis_won] * self.won)
        self.visualizer.append_caption(f", turn {self.turn}" +
                            f" and best solve in turn {self.fastest_win}"*(self.fastest_win!=DEFAULT_FASTEST_WIN))
        return stop

    def main(self):
        """keep the game running at 4 fps"""
        clock = pygame.time.Clock()
        while True:
            if not self.visualizer.handle_events([self.save_keys, self.listener]):
                return

            key = self.determine_key()
            self.handle_input(key)
            self.update_won()
            self.visualizer.visualize(self.maze, extra=[self.visualizer.vis_won] * self.won)
            self.visualizer.append_caption(f", turn {self.turn}" +
                                f" and best solve in turn {self.fastest_win}"*(self.fastest_win!=DEFAULT_FASTEST_WIN))
            clock.tick(4)

def main(filename=None):
    """initialize game and keep it running at 4 fps"""
    # shouldn't get used as everything goes via Controller or PlaySequence
    maze = Maze(ask_maze())
    visualizer = Visualizer(maze)
    game = Game(maze, visualizer)
    game.main()
    pygame.quit()

if __name__ == "__main__":
    print("Arrows move the red dots. Get them on the green squares to win. "
            "[, ] are undo and redo, 0 will reset.")
    from pgame.controller import Controller
    controller = Controller()
    controller.main()
