# This solver should work with ALL possible mazes, as long as y, x, height are
# the only element attributes able to change.

from utility import get_cmd_argument
from controls import GameControls
from game import *

state = None
ALLOWED_ACTIONS = (GameControls.UP, GameControls.DOWN, GameControls.LEFT, GameControls.RIGHT)

def state_to_tuple():
    players = []
    blocks = []

    for player in state.element_dict[Player]:
        players.append((player.y, player.x, player.height))
    for block in state.element_dict[Block]:
        blocks.append((block.y, block.x))

    return (frozenset(players), frozenset(blocks))

def tuple_to_state(tup):
    players, blocks = tup

    for player, info in zip(state.element_dict[Player], players):
        state.layout[player.y][player.x].remove(player)
        player.y, player.x, player.height = info
        state.layout[player.y][player.x].append(player)
    for block, info in zip(state.element_dict[Block], blocks):
        state.layout[block.y][block.x].remove(block)
        block.y, block.x = info
        state.layout[block.y][block.x].append(block)

def solve(maze):
    global state
    state = maze

    visited_states  = set()
    visiting_states = {state_to_tuple()}
    to_visit_states = set()

#     for i in range(2):
    while visiting_states:
        print("number of visited states:", len(visited_states))
        for tupl in visiting_states:
            for action in ALLOWED_ACTIONS:
                tuple_to_state(tupl)
                changed = False

                for subturn, actions in state.turn_functions.items():

                    # Which (element, method) pairs should be called this subturn.
                    to_do = set()
                    for (cls, method) in actions:
                        for element in state.element_dict[cls]:
                            to_do.add((element, method))

                    while to_do:

                        not_done = set()
                        for element, method in to_do:
                            if not method(element, action):
                                # The action could not be executed.
                                not_done.add((element, method))

                        if to_do == not_done:
                            # No actions could be executed this subturn.
                            break
                        else:
                            to_do = not_done
                            changed = True

                tup = state_to_tuple()
                if changed and tup not in visited_states:
                    for element in state.element_iter():
                        if not element.won():
                            break
                    else:
                        return True
                    to_visit_states.add(tup)

        #print(visited_states, visiting_states, to_visit_states)
        visited_states = visited_states.union(visiting_states)
        visiting_states = to_visit_states
        to_visit_states = set()
    return False

if __name__ == "__main__":
    import cProfile
    with cProfile.Profile() as pr:
        maze = Maze("wow")
        if solve(maze):
            print("1")
    pr.print_stats()
    #
    # maze = Maze(get_cmd_argument())
    # if solve(maze):
    #     print("jeh")
