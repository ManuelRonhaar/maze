from utility import get_files
from maze import *

def new_save(maze, mazename):
    file = open(f"new_layouts/{mazename}.txt", "wt")
    file.write(str(maze.width) + "\n")
    file.write(str(maze.height) + "\n")
    file.write(str(maze.get_nr_of_elements()) + "\n")

    for element_kind, elements in maze.element_dict.items():
        file.write(f"{element_kind.__name__} {len(elements)}\n")
        for element in elements:
            file.write(f"{element.y} {element.x} {element.height}{pack_data(element)}\n")
    # for element in self.element_iter():
    #     file.write(f"{element.__class__.__name__} {element.y} {element.x} {element.height} {pack_data(element)}\n")
    file.close()

for mazename in get_files():
    maze = Maze(mazename)
    maze.save(directory="new_layouts")
