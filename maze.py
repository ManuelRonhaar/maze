from copy import copy, deepcopy
from heapq import *

from elements import *
#from extra_elements import *
#from enums import *
from utility import *

# rename all maze to level maybe?
# move game won to here maybe?

class Maze:
    def __init__(self, name="oops", directory="layouts", size=()):
        self.name = name
        self.element_classes = self.get_element_classes()
        self.turn_functions = self.get_turn_functions()

        if exists(self.name, directory):
            self.init_maze_from_file(directory)
        else:
            self.generate_empty_maze(size)

    def init_maze_from_file(self, directory="layouts"):
        self.new = False
        with open(f"{directory}/{self.name}.txt") as file:
            self.width = int(file.readline())
            self.height = int(file.readline())
            nr_of_elements = int(file.readline())

            # List of elements for every y,x position
            self.layout = [[[] for _ in range(self.width)] for _ in range(self.height)]
            # Dictionary with a list for every element kind
            self.element_dict = {kind: [] for kind in self.element_classes}

            name_dict = self.map_name_to_class()
            while True:
                lst = file.readline().split(" ")
                if len(lst) == 2:
                    nr = int(lst[1])
                    name_dict[lst[0]].init(nr, file, self)
                else: break

    def update_layout_with_element_dict(self):
        self.layout = [[[] for _ in range(self.width)] for _ in range(self.height)]
        for element in self.element_iter():
            self.layout[element.y][element.x].append(element)

    def generate_empty_maze(self, size):
        self.new = True
        self.height, self.width = size
        self.element_dict = {kind: [] for kind in self.element_classes}
        for y in range(self.height):
            for x in range(self.width):
                element = Floor()
                element.maze = self
                element.y = y
                element.x = x
                element.height = 0
                self.element_dict[Floor].append(element)
        self.layout = [[[self.element_dict[Floor][y*self.width+x]] for x in range(self.width)] for y in range(self.height)]

    def in_maze(self, y, x):
        return 0 <= y < self.height and 0 <= x < self.width

    def element_iter(self):
        for kind, elements in self.element_dict.items():
            for element in elements:
                yield element

    def get_nr_of_elements(self):
        nr_of_elements = 0
        for kind, elements in self.element_dict.items():
            nr_of_elements += len(elements)
        return nr_of_elements

    def get_elements(self, y, x):
        return self.layout[y][x]

    def get_cls_elements_at(self, y, x, cls):
        elements = self.get_elements(y, x)
        cls_elements = set()
        for e in elements:
            if isinstance(e, cls):
                cls_elements.add(e)
        return cls_elements

    def get_cls_element_at(self, y, x, cls):
        elements = self.get_elements(y, x)
        for e in elements:
            if isinstance(e, cls):
                return e
        return None

    def __str__(self):
        for element in self.element_iter():
            if not isinstance(element, Floor):
                print(f"{element.__class__.__name__} {element.y} {element.x} {element.height} {pack_data(element)}")
        return("")

    def save(self, name=None, directory="layouts"):
        """save the maze with the specified filename"""
        if not name: name = self.name
        file = open(f"{directory}/{name}.txt", "wt")
        file.write(str(self.width) + "\n")
        file.write(str(self.height) + "\n")
        file.write(str(self.get_nr_of_elements()) + "\n")

        for element_kind, elements in self.element_dict.items():
            if len(elements) > 0:
                element_kind.save(file, elements)
        # for element in self.element_iter():
        #     file.write(f"{element.__class__.__name__} {element.y} {element.x} {element.height} {pack_data(element)}\n")
        file.close()

    def get_element_classes(self, element=BaseElement):
        subclasses = element.__subclasses__()
        # Might be cleaner to not extend loop while looping over it pythonic way.
        for subcls in subclasses:
            subclasses.extend(self.get_element_classes(subcls))
            #if subsub := self.get_element_classes(subcls):
            #    subclasses.extend(subsub)
        return subclasses

    def map_name_to_class(self):
        return dict((C.__name__, C) for C in self.get_element_classes())

    def get_turn_functions(self):
        d = dict()  # turn: ((cls, function), ..., (cls, function))
        for cls in self.element_classes:
            for turn, function in cls.controls:
                method = getattr(cls, function)
                if turn in d:
                    d[turn].append((cls, method))
                else:
                    d[turn] = [(cls, method)]

        turns = list(d.keys())
        turns.sort()

        turn_functions = {turn: d[turn] for turn in turns}
        return turn_functions
